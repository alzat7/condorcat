//
//  VotingRepository.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
class VotingRepository: BaseRepository, VotingRemoteDataManagerInputProtocol {
    
    var remoteRequestHandler: VotingRemoteDataManagerOutputProtocol?
    
    func getRandomBreed() {
        let url = endPoint.baseUrl + endPoint.searchRandomBreedImagesEndPoint
        createRequest(url: url)
        session.dataTask(with: request!){(data,response,error) in
            guard let data = data,error == nil , let result = response as?
                HTTPURLResponse else{
                    self.remoteRequestHandler?.errorRemote(error: error!)
                    return
            }
            
            if(result.statusCode == 200){
                do{
                    let decoder = JSONDecoder()
                    let breed = try decoder.decode([BreedVoteDto].self, from: data);
                    self.remoteRequestHandler?.remoteDataManagerCallBackData(result: breed[0])
                }catch{
                    self.remoteRequestHandler?.errorRemote(error: error)
                }
            }else{
                self.remoteRequestHandler?.errorRemote(error: error!)
            }
        }.resume()
    }
}
