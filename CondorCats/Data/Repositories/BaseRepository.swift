//
//  BaseRepository.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
class BaseRepository: NSObject {
    var endPoint = EndPoints()
    var apiConstants = ApiConstants()
    let session = URLSession.shared
    var request: URLRequest?
    
    func createRequest(url: String){
        request = URLRequest(url: URL(string: url)!)
        request?.httpMethod = apiConstants.getMethod
        request?.setValue(apiConstants.config, forHTTPHeaderField: apiConstants.contentType)
    }
}
