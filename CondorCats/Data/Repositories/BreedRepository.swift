//
//  BreedRepository.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
class BreedRepository: BaseRepository, MainRemoteDataManagerInputProtocol {
    var remoteRequestHandler: MainRemoteDataManagerOutputProtocol?
    var remoteRequestHandlerDetail: BreedDetailRemoteDataManagerOutputProtocol?
    
    func getBreeds() {
        let url = endPoint.baseUrl + endPoint.breedListEndPoint
        createRequest(url: url)
        session.dataTask(with: request!){(data,response,error) in
            guard let data = data,error == nil , let result = response as?
                HTTPURLResponse else{
                    self.remoteRequestHandler?.errorRemote(error: error!)
                    return
            }
            
            if(result.statusCode == 200){
                do{
                    let decoder = JSONDecoder()
                    let breed = try decoder.decode([BreedDto].self, from: data);
                    self.remoteRequestHandler?.remoteDataManagerCallBackData(result: breed)
                }catch{
                    self.remoteRequestHandler?.errorRemote(error: error)
                }
            }else{
                self.remoteRequestHandler?.errorRemote(error: error!)
            }
        }.resume()
    }
}

extension BreedRepository: BreedDetailRemoteDataManagerInputProtocol{
    
    func getDetail(breed: Breed) {
        let detailEndPoint = String(format: endPoint.fetchBreedByIdEndPoint, breed.id)
        let url = endPoint.baseUrl + detailEndPoint
        createRequest(url: url)
        session.dataTask(with: request!){(data, response, error) in
            guard let data = data, error == nil, let result = response as? HTTPURLResponse else{
                self.remoteRequestHandler?.errorRemote(error: error!)
                return
            }
            
            if(result.statusCode == 200){
                do{
                    let decoder = JSONDecoder()
                    let breed = try decoder.decode([BreedDetailDto].self, from: data);
                    self.remoteRequestHandlerDetail?.remoteDataManagerCallBackData(result: breed[0])
                }catch{
                    self.remoteRequestHandlerDetail?.errorRemote(error: error)
                }
            }else{
                self.remoteRequestHandlerDetail?.errorRemote(error: error!)
            }
        }.resume()
    }
}
