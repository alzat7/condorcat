//
//  ApiConstants.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
class ApiConstants {
    let getMethod = "Get"
    let contentType = "Content-Type"
    let config = "application/json; charset=utf8"
}
