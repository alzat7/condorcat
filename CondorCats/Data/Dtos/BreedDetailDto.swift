//
//  BreedDetailDto.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
struct BreedDetailDto: Codable {
    var breeds: [BreedDto]
    var imageUrl: String
    
    enum CodingKeys: String,CodingKey {
        case breeds, imageUrl = "url"
    }
}
