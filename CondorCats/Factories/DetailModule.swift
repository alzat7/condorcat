//
//  DetailModule.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
import UIKit
class DetailModule {
    class func create(data breed: Breed) -> UIViewController{
        let breedDetailView = detailView
        if let view = breedDetailView as? BreedDetailView {
            let presenter: BreedDetailPresenterProtocol & BreedDetailInteractorOutputProtocol = BreedDetailPresenter()
            let interactor: BreedDetailInteractorInputProtocol & BreedDetailRemoteDataManagerOutputProtocol = BreedDetailInteractor()
            let remoteDataManager: BreedDetailRemoteDataManagerInputProtocol = BreedRepository()
            let router: BreedDetailRouterProtocol = BreedDetailRouter()
            
            view.presenter = presenter
            presenter.view = view
            presenter.router = router
            presenter.interactor = interactor
            presenter.breed = breed
            interactor.presenter = presenter
            interactor.remoteDatamanager = remoteDataManager
            remoteDataManager.remoteRequestHandlerDetail = interactor
            
            return breedDetailView
        }
        return UIViewController()
    }
    
    static var detailView: UIViewController {
        return BreedDetailView(nibName: "BreedDetail", bundle: Bundle.main)
    }
}
