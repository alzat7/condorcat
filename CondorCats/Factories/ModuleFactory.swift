//
//  ModuleFactory.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
import UIKit
class ModuleFactory {
    
    var newModuleViewController: UIViewController?
    
    func createModule(typeOfModule module: ModuleEnumeration, args data: Breed? = nil) -> UIViewController {
        
        switch module {
            
        case ModuleEnumeration.Main:
            newModuleViewController = MainModule.create()
            break
        case ModuleEnumeration.Detail:
            newModuleViewController = DetailModule.create(data: data!)
            break
        default:
            newModuleViewController = VoteModule.create()
            break
        }
        
        return newModuleViewController!
    }
}
