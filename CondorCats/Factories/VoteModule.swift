//
//  VoteModule.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
import UIKit
class VoteModule {
    
    class func create() -> UIViewController {
        let navController = votedView
        if let view = navController as? VotingView {
            let presenter: VotingPresenterProtocol & VotingInteractorOutputProtocol = VotingPresenter()
            let interactor: VotingInteractorInputProtocol & VotingRemoteDataManagerOutputProtocol = VotingInteractor()
            let remoteDataManager: VotingRemoteDataManagerInputProtocol = VotingRepository()
            let router: VotingRouterProtocol = VotingRouter()
            
            view.presenter = presenter
            presenter.view = view
            presenter.router = router
            presenter.interactor = interactor
            interactor.presenter = presenter
            interactor.remoteDatamanager = remoteDataManager
            remoteDataManager.remoteRequestHandler = interactor
            
            return navController
        }
        return UIViewController()
    }
    
    static var votedView: UIViewController {
        return VotingView(nibName: "Voting", bundle: Bundle.main)
    }
}
