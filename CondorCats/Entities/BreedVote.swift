//
//  BreedVote.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
struct BreedVote: Codable {
    var id: String
    var imageUrl: String
    
    init(id: String, imageUrl: String) {
        self.id = id
        self.imageUrl = imageUrl
    }
}
