//
//  BreedDetail.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
struct BreedDetail: Codable {
    var breed: Breed
    var imageUrl: String
    
    init(breed: Breed, imageUrl: String) {
        self.breed = breed
        self.imageUrl = imageUrl
    }
}
