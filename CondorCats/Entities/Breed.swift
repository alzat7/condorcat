//
//  Breed.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
struct Breed: Codable {
    var id: String
    var name: String
    var temperament: String?
    var origin: String?
    var countryCode: String?
    var description: String?
    var wikipediaurl: String?
    
    init(id: String, name: String, temperament: String, origin: String, countryCode: String, description: String, wikipediaurl: String) {
        self.id = id
        self.name = name
        self.temperament = temperament
        self.origin = origin
        self.countryCode = countryCode
        self.description = description
        self.wikipediaurl = wikipediaurl
    }
}
