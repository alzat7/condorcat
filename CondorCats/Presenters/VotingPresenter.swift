//
//  VotingPresenter.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation

class VotingPresenter  {
    
    // MARK: Properties
    weak var view: VotingViewProtocol?
    var interactor: VotingInteractorInputProtocol?
    var router: VotingRouterProtocol?
    
}

extension VotingPresenter: VotingPresenterProtocol {
    // TODO: implement presenter methods
    func viewDidLoad() {
        interactor?.getRandomBreed()
    }
}

extension VotingPresenter: VotingInteractorOutputProtocol {
    func interactorDataManagerCallBackData(result breedVote: BreedVote) {
        view?.presenterDataManagerCallBackData(result: breedVote)
    }
    
    func errorRemote(error: Error) {
        view?.errorRemote(error: error)
    }
}
