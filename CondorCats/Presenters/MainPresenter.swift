//
//  MainPresenter.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation

class MainPresenter  {
    
    // MARK: Properties
    weak var view: MainViewProtocol?
    var interactor: MainInteractorInputProtocol?
    var router: MainRouterProtocol?
    
}

extension MainPresenter: MainPresenterProtocol {
    
    func viewDidLoad() {
        interactor?.getBreeds()
    }
    
    func showDetailView(with breed: Breed) {
        router?.showDetailView(from: view!, withData: breed)
    }
    
    func showVotingView() {
        router?.showVotingView(from: view!)
    }
}

extension MainPresenter: MainInteractorOutputProtocol {
    
    func interactorCallBackData(result breeds: [Breed]) {
        view?.presenterCallBackData(result: breeds)
    }
    
    func errorRemote(error: Error) {
        view?.errorRemote(error: error)
    }
}
