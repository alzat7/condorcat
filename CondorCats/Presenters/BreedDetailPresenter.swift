//
//  BreedDetailPresenter.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation

class BreedDetailPresenter  {
    
    // MARK: Properties
    weak var view: BreedDetailViewProtocol?
    var interactor: BreedDetailInteractorInputProtocol?
    var router: BreedDetailRouterProtocol?
    var breed: Breed?
}

extension BreedDetailPresenter: BreedDetailPresenterProtocol {
    
    // TODO: implement presenter methods
    func viewDidLoad() {
        interactor?.getDetail(breed: self.breed!)
    }
}

extension BreedDetailPresenter: BreedDetailInteractorOutputProtocol {
    func interactorDataManagerCallBackData(result breedDetail: BreedDetail) {
        view?.presenterCallBackData(result: breedDetail)
    }
    
    func errorRemote(error: Error) {
        view?.errorRemote(error: error)
    }
}
