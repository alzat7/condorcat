//
//  VotingView.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
import UIKit

class VotingView: BaseViewController {

    // MARK: Properties
    @IBOutlet weak var breedImageView: UIImageView!
    @IBOutlet weak var card: UIView!
    
    var presenter: VotingPresenterProtocol?
    var divistor: CGFloat!

    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
        divistor = (view.frame.width / 2) / 0.61
    }
    
    @IBAction func panCard(_ sender: UIPanGestureRecognizer) {
        let card = sender.view!
        let point = sender.translation(in: view)
        let xFromCenter = card.center.x - view.center.x
        let scale = min(100/abs(xFromCenter), 1)
            
        card.center = CGPoint(x: view.center.x + point.x, y: view.center.y + point.y)
        card.transform = CGAffineTransform(rotationAngle: xFromCenter/divistor).scaledBy(x: scale, y: scale)
        
        if sender.state == UIGestureRecognizer.State.ended {
            
            if card.center.x < 75 {
                UIView.animate(withDuration: 0.3, animations: {
                    card.center = CGPoint(x: card.center.x - 200, y: card.center.y)
                    card.alpha = 0
                })
                showNewVote()
                return
            }else if card.center.x > (view.frame.width - 75){
                UIView.animate(withDuration: 0.3, animations: {
                    card.center = CGPoint(x: card.center.x + 200, y: card.center.y)
                    card.alpha = 0
                })
                showNewVote()
                return
            }
            restarViewPosition()
        }
    }
    
    func restarViewPosition() {
        UIView.animate(withDuration: 0.2, animations: {
            self.originalPosition()
        })
    }
    
    func showNewVote() {
        presenter?.viewDidLoad()
        self.breedImageView.image = nil
        originalPosition()
    }
    
    func originalPosition() {
        self.card.center = self.view.center
        self.card.alpha = 1
        self.card.transform = CGAffineTransform.identity
    }
}

extension VotingView: VotingViewProtocol {
    func presenterDataManagerCallBackData(result breedVote: BreedVote) {
        DispatchQueue.main.async {
            self.breedImageView.downloadImage(from: URL(string: breedVote.imageUrl)!)
        }
    }
    
    func errorRemote(error: Error) {
        DispatchQueue.main.async {
            self.showAlertDialog(error: error)
        }
    }
}
