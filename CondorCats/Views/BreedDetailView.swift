//
//  BreedDetailView.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
import UIKit

class BreedDetailView: BaseViewController {

    // MARK: Properties
    @IBOutlet weak var breedImageView: UIImageView!
    @IBOutlet weak var breedNameLabel: UILabel!
    @IBOutlet weak var breedOriginLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var temperamentLabel: UILabel!
    
    var presenter: BreedDetailPresenterProtocol?
    var detail: BreedDetail?

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        let play = UIBarButtonItem(title: "More Info", style: .plain, target: self, action: #selector(wikiTouch))
        navigationItem.rightBarButtonItems = [play]
        presenter?.viewDidLoad()
    }
    
    @IBAction func wikiTouch(_ sender: UIButton) {
        if let url = URL(string: detail!.breed.wikipediaurl!) {
           UIApplication.shared.open(url)
       }
    }
}

extension BreedDetailView: BreedDetailViewProtocol {
    func presenterCallBackData(result breedDetail: BreedDetail) {
        DispatchQueue.main.async {
            self.detail = breedDetail
            self.breedNameLabel.text = breedDetail.breed.name
            self.breedOriginLabel.text = "\(breedDetail.breed.origin!) - \(breedDetail.breed.countryCode!)"
            self.descriptionLabel.text = breedDetail.breed.description
            self.temperamentLabel.text = breedDetail.breed.temperament
            self.breedImageView.downloadImage(from: URL(string: breedDetail.imageUrl)!)
        }
    }
    
    func errorRemote(error: Error) {
        DispatchQueue.main.async {
            self.showAlertDialog(error: error)
        }
    }
}
