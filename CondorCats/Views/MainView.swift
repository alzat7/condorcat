//
//  MainView.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
import UIKit

class MainView: BaseViewController, UITableViewDataSource, UITableViewDelegate{
    
    // MARK: Properties
    @IBOutlet weak var breedListTableView: UITableView!
    var presenter: MainPresenterProtocol?
    var breedList = [Breed]()

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        let play = UIBarButtonItem(title: "Vote", style: .plain, target: self, action: #selector(showVotingView))
        navigationItem.rightBarButtonItems = [play]
        presenter?.viewDidLoad()
    }
    
    @IBAction func showVotingView(_ sender: UIButton) {
        presenter?.showVotingView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return breedList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.textLabel?.text = breedList[indexPath.row].name
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.showDetailView(with: breedList[indexPath.row])
    }
}

extension MainView: MainViewProtocol {
    func presenterCallBackData(result: [Breed]) {
        DispatchQueue.main.async {
            self.breedList = result
            self.breedListTableView.reloadData()
        }
    }
    
    func errorRemote(error: Error) {
        DispatchQueue.main.async {
            self.showAlertDialog(error: error)
        }
    }
}
