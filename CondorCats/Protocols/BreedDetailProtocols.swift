//
//  BreedDetailProtocols.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
import UIKit

protocol BreedDetailViewProtocol: class {
    // PRESENTER -> VIEW
    var presenter: BreedDetailPresenterProtocol? { get set }
    
    func presenterCallBackData(result breedDetail: BreedDetail)
    func errorRemote(error : Error)
}

protocol BreedDetailRouterProtocol: class {
    // PRESENTER -> ROUTER
    static func createBreedDetailModule(data breed: Breed) -> UIViewController
}

protocol BreedDetailPresenterProtocol: class {
    // VIEW -> PRESENTER
    var view: BreedDetailViewProtocol? { get set }
    var interactor: BreedDetailInteractorInputProtocol? { get set }
    var router: BreedDetailRouterProtocol? { get set }
    var breed: Breed? { get set }
    
    func viewDidLoad()
}

protocol BreedDetailInteractorOutputProtocol: class {
// INTERACTOR -> PRESENTER
    func interactorDataManagerCallBackData(result breedDetail: BreedDetail)
    func errorRemote(error : Error)
}

protocol BreedDetailInteractorInputProtocol: class {
    // PRESENTER -> INTERACTOR
    var presenter: BreedDetailInteractorOutputProtocol? { get set }
    var remoteDatamanager: BreedDetailRemoteDataManagerInputProtocol? { get set }
    
    func getDetail(breed: Breed)
}

protocol BreedDetailDataManagerInputProtocol: class {
    // INTERACTOR -> DATAMANAGER
}

protocol BreedDetailRemoteDataManagerInputProtocol: class {
    // INTERACTOR -> REMOTEDATAMANAGER
    var remoteRequestHandlerDetail: BreedDetailRemoteDataManagerOutputProtocol? { get set }
    
    func getDetail(breed: Breed)
}

protocol BreedDetailRemoteDataManagerOutputProtocol: class {
    // REMOTEDATAMANAGER -> INTERACTOR
    func remoteDataManagerCallBackData(result breedDetailDto: BreedDetailDto)
    func errorRemote(error : Error)
}
