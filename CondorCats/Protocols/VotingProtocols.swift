//
//  VotingProtocols.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
import UIKit

protocol VotingViewProtocol: class {
    // PRESENTER -> VIEW
    var presenter: VotingPresenterProtocol? { get set }
    
    func presenterDataManagerCallBackData(result breedVote: BreedVote)
    func errorRemote(error : Error)
}

protocol VotingRouterProtocol: class {
    // PRESENTER -> ROUTER
    static func createVotingModule() -> UIViewController
}

protocol VotingPresenterProtocol: class {
    // VIEW -> PRESENTER
    var view: VotingViewProtocol? { get set }
    var interactor: VotingInteractorInputProtocol? { get set }
    var router: VotingRouterProtocol? { get set }
    
    func viewDidLoad()
}

protocol VotingInteractorOutputProtocol: class {
    // INTERACTOR -> PRESENTER
    func interactorDataManagerCallBackData(result breedVote: BreedVote)
    func errorRemote(error : Error)
}

protocol VotingInteractorInputProtocol: class {
    // PRESENTER -> INTERACTOR
    var presenter: VotingInteractorOutputProtocol? { get set }
    var remoteDatamanager: VotingRemoteDataManagerInputProtocol? { get set }
    
    func getRandomBreed()
}

protocol VotingDataManagerInputProtocol: class {
    // INTERACTOR -> DATAMANAGER
}

protocol VotingRemoteDataManagerInputProtocol: class {
    // INTERACTOR -> REMOTEDATAMANAGER
    var remoteRequestHandler: VotingRemoteDataManagerOutputProtocol? { get set }
    
    func getRandomBreed()
}

protocol VotingRemoteDataManagerOutputProtocol: class {
    // REMOTEDATAMANAGER -> INTERACTOR
    func remoteDataManagerCallBackData(result breedVoteDto: BreedVoteDto)
    func errorRemote(error : Error)
}
