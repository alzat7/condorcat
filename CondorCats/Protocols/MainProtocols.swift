//
//  MainProtocols.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
import UIKit

protocol MainViewProtocol: class {
    // PRESENTER -> VIEW
    var presenter: MainPresenterProtocol? { get set }
    
    func presenterCallBackData(result: [Breed])
    func errorRemote(error : Error)
}

protocol MainRouterProtocol: class {
    // PRESENTER -> ROUTER
    static func createMainModule() -> UIViewController
    
    func showDetailView(from view: MainViewProtocol, withData: Breed)
    func showVotingView(from view: MainViewProtocol)
}

protocol MainPresenterProtocol: class {
    // VIEW -> PRESENTER
    var view: MainViewProtocol? { get set }
    var interactor: MainInteractorInputProtocol? { get set }
    var router: MainRouterProtocol? { get set }
    
    func viewDidLoad()
    func showDetailView(with breed: Breed)
    func showVotingView()
}

protocol MainInteractorOutputProtocol: class {
    // INTERACTOR -> PRESENTER
    func interactorCallBackData(result breeds: [Breed])
    func errorRemote(error : Error)
}

protocol MainInteractorInputProtocol: class {
    // PRESENTER -> INTERACTOR
    var presenter: MainInteractorOutputProtocol? { get set }
    var remoteDatamanager: MainRemoteDataManagerInputProtocol? { get set }
    
    func getBreeds()
}

protocol MainDataManagerInputProtocol: class {
    // INTERACTOR -> DATAMANAGER
}

protocol MainRemoteDataManagerInputProtocol: class {
    // INTERACTOR -> REMOTEDATAMANAGER
    var remoteRequestHandler: MainRemoteDataManagerOutputProtocol? { get set }
    
    func getBreeds()
}

protocol MainRemoteDataManagerOutputProtocol: class {
    // REMOTEDATAMANAGER -> INTERACTOR
    func remoteDataManagerCallBackData(result breeds: [BreedDto])
    func errorRemote(error : Error)
}
