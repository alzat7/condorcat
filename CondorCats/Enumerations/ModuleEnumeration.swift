//
//  ModuleEnumeration.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
enum ModuleEnumeration {
    case Main
    case Detail
    case Voting
}
