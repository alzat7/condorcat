//
//  MainRouter.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
import UIKit

class MainRouter: MainRouterProtocol {
    
    class func createMainModule() -> UIViewController {
        let module = ModuleFactory()
        return module.createModule(typeOfModule: ModuleEnumeration.Main)
    }
    
    func showDetailView(from view: MainViewProtocol, withData: Breed) {
        let newDetailView = BreedDetailRouter.createBreedDetailModule(data: withData)
        
        if let detailView = view as? UIViewController {
            detailView.navigationController?.pushViewController(newDetailView, animated: true)
        }
    }
    
    func showVotingView(from view: MainViewProtocol) {
        let newVotingView = VotingRouter.createVotingModule()
        
        if let votingView = view as? UIViewController {
            votingView.navigationController?.present(newVotingView, animated: true, completion: nil)
        }
    }
}
