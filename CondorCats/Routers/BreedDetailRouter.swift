//
//  BreedDetailRouter.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation
import UIKit

class BreedDetailRouter: BreedDetailRouterProtocol {

    class func createBreedDetailModule(data breed: Breed) -> UIViewController {        
        let module = ModuleFactory()
        return module.createModule(typeOfModule: ModuleEnumeration.Detail, args: breed)
    }
}
