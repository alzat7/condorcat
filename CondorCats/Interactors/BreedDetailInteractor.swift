//
//  BreedDetailInteractor.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation

class BreedDetailInteractor: BreedDetailInteractorInputProtocol {
    
    // MARK: Properties
    weak var presenter: BreedDetailInteractorOutputProtocol?
    var remoteDatamanager: BreedDetailRemoteDataManagerInputProtocol?
    
    func getDetail(breed: Breed) {
        remoteDatamanager?.getDetail(breed: breed)
    }
}

extension BreedDetailInteractor: BreedDetailRemoteDataManagerOutputProtocol {
    func remoteDataManagerCallBackData(result breedDetailDto: BreedDetailDto) {
        let breed = Breed(id: breedDetailDto.breeds[0].id,
                          name: breedDetailDto.breeds[0].name,
                          temperament: breedDetailDto.breeds[0].temperament!,
                          origin: breedDetailDto.breeds[0].origin!,
                          countryCode: breedDetailDto.breeds[0].countryCode!,
                          description: breedDetailDto.breeds[0].description!,
                          wikipediaurl: breedDetailDto.breeds[0].wikipediaurl ?? "")
        
        presenter?.interactorDataManagerCallBackData(result: BreedDetail(breed: breed, imageUrl: breedDetailDto.imageUrl))
    }
    
    func errorRemote(error: Error) {
        presenter?.errorRemote(error: error)
    }
}
