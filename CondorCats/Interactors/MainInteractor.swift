//
//  MainInteractor.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation

class MainInteractor: MainInteractorInputProtocol {
   
    // MARK: Properties
    weak var presenter: MainInteractorOutputProtocol?
    var remoteDatamanager: MainRemoteDataManagerInputProtocol?
    var breedList = [Breed]()
    
    func getBreeds() {
        remoteDatamanager?.getBreeds()
    }
}

extension MainInteractor: MainRemoteDataManagerOutputProtocol {
    
    func remoteDataManagerCallBackData(result breeds: [BreedDto]) {
        for breed in breeds {
            let breedItem = Breed(id: breed.id,
                                  name: breed.name,
                                  temperament: breed.temperament!,
                                  origin: breed.origin!,
                                  countryCode: breed.countryCode!,
                                  description: breed.description!,
                                  wikipediaurl: breed.wikipediaurl ?? "")
            breedList.append(breedItem)
        }

        presenter?.interactorCallBackData(result: breedList)
    }
    
    func errorRemote(error: Error) {
        presenter?.errorRemote(error: error)
    }
}
