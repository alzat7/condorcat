//
//  VotingInteractor.swift
//  CondorCats
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import Foundation

class VotingInteractor: VotingInteractorInputProtocol {
    
    // MARK: Properties
    weak var presenter: VotingInteractorOutputProtocol?
    var remoteDatamanager: VotingRemoteDataManagerInputProtocol?

    func getRandomBreed() {
        remoteDatamanager?.getRandomBreed()
    }
}

extension VotingInteractor: VotingRemoteDataManagerOutputProtocol {
    func remoteDataManagerCallBackData(result breedVoteDto: BreedVoteDto) {
        let breedVote = BreedVote(id: breedVoteDto.id, imageUrl: breedVoteDto.imageUrl)
        presenter?.interactorDataManagerCallBackData(result: breedVote)
    }
    
    func errorRemote(error: Error) {
        presenter?.errorRemote(error: error)
    }
}
