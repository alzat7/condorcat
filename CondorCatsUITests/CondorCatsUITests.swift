//
//  CondorCatsUITests.swift
//  CondorCatsUITests
//
//  Created by Alejandro  Alzate on 12/10/19.
//  Copyright © 2019 Alejandro  Alzate. All rights reserved.
//

import XCTest

class CondorCatsUITests: XCTestCase {
    
    var app: XCUIApplication!

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        app = XCUIApplication()
        app.launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // UI tests must launch the application that they test.
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testAllApplication() {
        
        let app = XCUIApplication()
        app.tables["tableView"]/*@START_MENU_TOKEN@*/.staticTexts["Abyssinian"]/*[[".cells.staticTexts[\"Abyssinian\"]",".staticTexts[\"Abyssinian\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.navigationBars["CondorCats.BreedDetailView"].buttons["Back"].tap()
        app.navigationBars["CondorCats.MainView"].buttons["Vote"].tap()
        
        let cardviewElement = app/*@START_MENU_TOKEN@*/.otherElements["cardView"]/*[[".otherElements[\"alaverga\"].otherElements[\"cardView\"]",".otherElements[\"cardView\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        cardviewElement.swipeRight()
        cardviewElement.swipeLeft()
        cardviewElement.swipeRight()
        cardviewElement/*@START_MENU_TOKEN@*/.swipeLeft()/*[[".swipeUp()",".swipeLeft()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        app.otherElements["voteContainerView"].swipeDown()
        
        
    }
    
    func testShowVoteView() {
        
        let app = XCUIApplication()
        app.navigationBars["CondorCats.MainView"].buttons["Vote"].tap()
        
        let cardviewElement = app/*@START_MENU_TOKEN@*/.otherElements["cardView"]/*[[".otherElements[\"alaverga\"].otherElements[\"cardView\"]",".otherElements[\"cardView\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        cardviewElement.swipeRight()
        cardviewElement.swipeLeft()
        cardviewElement.swipeRight()
        
        let voteContainerViewElement = app.otherElements["voteContainerView"]
        voteContainerViewElement.swipeDown()
        voteContainerViewElement.swipeDown()
        
 
    }
    
    func testShowWikipedia() {
        
        let app = XCUIApplication()
        app.tables["tableView"]/*@START_MENU_TOKEN@*/.staticTexts["Abyssinian"]/*[[".cells.staticTexts[\"Abyssinian\"]",".staticTexts[\"Abyssinian\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        
        let condorcatsBreeddetailviewNavigationBar = app.navigationBars["CondorCats.BreedDetailView"]
        condorcatsBreeddetailviewNavigationBar.buttons["More Info"].tap()
        
    }

    func testLaunchPerformance() {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
                XCUIApplication().launch()
            }
        }
    }
}
